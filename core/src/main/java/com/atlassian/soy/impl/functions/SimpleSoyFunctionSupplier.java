package com.atlassian.soy.impl.functions;

import com.atlassian.soy.renderer.SoyFunction;
import com.atlassian.soy.spi.functions.SoyFunctionSupplier;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A simple implementation of SoyFunctionSupplier which accepts an iterable of {@link SoyFunction}
 * in its constructor
 *
 * @since 2.4
 */
public class SimpleSoyFunctionSupplier implements SoyFunctionSupplier {

    private final Iterable<SoyFunction> functions;

    public SimpleSoyFunctionSupplier(Iterable<SoyFunction> functions) {
        this.functions = checkNotNull(functions, "functions");
    }

    @Override
    public Iterable<SoyFunction> get() {
        return functions;
    }
}
