package com.atlassian.soy.impl.modules;

import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import javax.inject.Inject;

import org.junit.Test;
import com.google.common.collect.Collections2;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.template.soy.data.SoyCustomValueConverter;
import com.google.template.soy.shared.restricted.SoyFunction;

import com.atlassian.soy.spi.i18n.I18nResolver;
import com.atlassian.soy.spi.i18n.JsLocaleResolver;
import com.atlassian.soy.spi.web.WebContextProvider;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class CoreFunctionsModuleTest {
    @Test
    public void testSoyFunctionNameMatchesBoundSoyFunctions() throws Exception {
        Injector injector = Guice.createInjector(
                new BridgeModule(
                        mock(SoyCustomValueConverter.class),
                        mock(I18nResolver.class),
                        mock(JsLocaleResolver.class),
                        new Properties(),
                        mock(WebContextProvider.class)),
                new CoreFunctionsModule());
        SoyFunctionCollector collector = new SoyFunctionCollector();
        injector.injectMembers(collector);
        HashSet<String> registeredFunctionNames =
                new HashSet<>(Collections2.transform(collector.values, SoyFunction::getName));
        assertEquals(registeredFunctionNames, CoreFunctionsModule.CORE_FUNCTION_NAMES);
    }

    static class SoyFunctionCollector {
        @Inject
        Set<SoyFunction> values;
    }
}
