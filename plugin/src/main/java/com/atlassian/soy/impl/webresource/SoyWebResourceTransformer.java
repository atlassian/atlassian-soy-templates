package com.atlassian.soy.impl.webresource;

import com.google.common.base.Supplier;

import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.transformer.CharSequenceDownloadableResource;
import com.atlassian.soy.impl.SoyManager;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.transformer.TransformableResource;
import com.atlassian.webresource.spi.transformer.UrlReadingWebResourceTransformer;

/**
 * @since 2.4
 */
public class SoyWebResourceTransformer implements UrlReadingWebResourceTransformer {
    private final SoyManager soyManager;
    private final ThreadLocalQueryParamsResolver queryParamsResolver;

    public SoyWebResourceTransformer(SoyManager soyManager, ThreadLocalQueryParamsResolver queryParamsResolver) {
        this.soyManager = soyManager;
        this.queryParamsResolver = queryParamsResolver;
    }

    @Override
    public DownloadableResource transform(TransformableResource transformableResource, QueryParams queryParams) {
        return new SoyDownloadableResource(
                transformableResource.nextResource(),
                transformableResource.location().getLocation(),
                queryParams);
    }

    private class SoyDownloadableResource extends CharSequenceDownloadableResource {

        private final String location;
        private final QueryParams queryParams;

        private SoyDownloadableResource(DownloadableResource nextResource, String location, QueryParams queryParams) {
            super(nextResource);
            this.location = location;
            this.queryParams = queryParams;
        }

        @Override
        protected CharSequence transform(final CharSequence originalContent) {
            return queryParamsResolver.withQueryParams(queryParams, new Supplier<CharSequence>() {
                @Override
                public CharSequence get() {
                    return soyManager.compile(originalContent, location);
                }
            });
        }
    }
}
