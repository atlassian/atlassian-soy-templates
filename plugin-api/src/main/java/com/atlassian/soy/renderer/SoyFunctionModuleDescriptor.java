package com.atlassian.soy.renderer;

import java.util.Collections;

import com.atlassian.annotations.Internal;
import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.util.validation.ValidationException;

@PublicApi
public class SoyFunctionModuleDescriptor extends AbstractModuleDescriptor<SoyFunction> {
    @SuppressWarnings("UnusedDeclaration") // Public API
    public static final String XML_ELEMENT_NAME = "soy-function";

    private SoyFunction module;

    @Internal
    public SoyFunctionModuleDescriptor(ModuleFactory factory) {
        super(factory);
    }

    @Override
    public void enabled() {
        super.enabled();
        module = moduleFactory.createModule(moduleClassName, this);
    }

    @Override
    public void disabled() {
        super.disabled();
        module = null;
    }

    @Override
    public SoyFunction getModule() {
        return module;
    }

    @Override
    protected void validate(final Element element) {
        super.validate(element);
        if (element.attributeValue("class") == null) {
            throw new ValidationException(
                    "There were validation errors:", Collections.singletonList("The class is required"));
        }
    }
}
