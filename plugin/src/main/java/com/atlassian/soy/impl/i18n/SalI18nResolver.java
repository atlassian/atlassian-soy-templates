package com.atlassian.soy.impl.i18n;

import java.io.Serializable;
import java.util.Locale;

import com.atlassian.soy.spi.i18n.I18nResolver;

/**
 * Simple SAL implementation which delegates to a SAL I18nResolver.
 *
 * @since 2.3
 */
public class SalI18nResolver implements I18nResolver {
    private final com.atlassian.sal.api.message.I18nResolver delegate;

    public SalI18nResolver(com.atlassian.sal.api.message.I18nResolver delegate) {
        this.delegate = delegate;
    }

    @Override
    public String getText(String key) {
        return delegate.getText(key);
    }

    @Override
    public String getText(String key, Serializable... serializables) {
        return delegate.getText(key, serializables);
    }

    @Override
    public String getText(Locale locale, String key) {
        return delegate.getText(locale, key);
    }

    @Override
    public String getRawText(Locale locale, String key) {
        return delegate.getRawText(locale, key);
    }
}
