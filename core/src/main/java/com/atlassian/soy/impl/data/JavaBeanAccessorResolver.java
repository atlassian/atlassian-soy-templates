package com.atlassian.soy.impl.data;

import java.lang.reflect.Method;
import java.util.Map;

// caches accessors of the classes
public interface JavaBeanAccessorResolver {

    void clearCaches();

    Map<String, Method> resolveAccessors(Class<?> targetClass);
}
