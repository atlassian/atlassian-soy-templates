package it.com.atlassian.soy.test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

import org.junit.Before;
import org.junit.Test;

import static java.net.http.HttpClient.newHttpClient;
import static org.junit.Assert.assertEquals;

public class SanityPluginTest {

    private String baseUrl;
    private String contextPath;
    private HttpClient client = newHttpClient();

    @Before
    public void setUp() throws Exception {
        baseUrl = System.getProperty("baseurl", "http://localhost:5990/refapp");
        contextPath = System.getProperty("context.path", "/refapp");
    }

    @Test
    public void testSanityPage() throws Exception {
        String body = sendRequest("sanity.sanityTest");

        assertEquals(
                "<!DOCTYPE html>" + "<html lang=\"en\">"
                        + "<head>"
                        + "<meta charset=\"utf-8\" />"
                        + "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=EDGE\">"
                        + "<title>You&#39;re on "
                        + contextPath + "</title>" + "</head>"
                        + "<body>Hello, Slim Shady!</body>"
                        + "</html>",
                body);
    }

    @Test
    public void testSanityPageWithRecord() throws Exception {
        String body = sendRequest("sanity.recordTest");

        assertEquals(
                "<!DOCTYPE html>" + "<html lang=\"en\">"
                        + "<head>"
                        + "<meta charset=\"utf-8\" />"
                        + "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=EDGE\">"
                        + "<title>You&#39;re on "
                        + contextPath + "</title>" + "</head>"
                        + "<body>Will the real Slim Shady please stand up?</body>"
                        + "</html>",
                body);
    }

    private String sendRequest(String template) throws URISyntaxException, IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI(baseUrl + "/plugins/servlet/soy-sanity?template=" + template))
                .build();

        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
        assertEquals(200, response.statusCode());
        return response.body();
    }
}
