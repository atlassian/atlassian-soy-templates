package it.com.atlassian.soy.test;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

public class StdOutMatcher extends FeatureMatcher<ProcessResult, String> {
    public StdOutMatcher(Matcher<? super String> subMatcher) {
        super(subMatcher, "output matches", "output");
    }

    @Override
    protected String featureValueOf(ProcessResult actual) {
        return actual.getOutput();
    }

    public static StdOutMatcher hasOutput(Matcher<? super String> subMatcher) {
        return new StdOutMatcher(subMatcher);
    }
}
