package com.atlassian.soy.test;

import java.io.IOException;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.base.Throwables;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

@UnrestrictedAccess
public class SanityServlet extends HttpServlet {

    private static final String MODULE_KEY =
            "com.atlassian.soy.atlassian-soy-plugin-integration-tests:soy-sanity-templates";

    private final SoyTemplateRenderer soyTemplateRenderer;

    public SanityServlet(SoyTemplateRenderer soyTemplateRenderer) {
        this.soyTemplateRenderer = soyTemplateRenderer;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        try {
            String template = req.getParameter("template");
            soyTemplateRenderer.render(
                    resp.getWriter(), MODULE_KEY, template, Map.of("person", new Person("Slim", "Shady")));
        } catch (SoyException e) {
            Throwables.propagateIfInstanceOf(e.getCause(), IOException.class);
            throw new ServletException(e);
        }
    }

    public record Person(String firstName, String lastName) {}
}
