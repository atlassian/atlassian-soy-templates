package com.atlassian.soy.impl.modules;

import java.util.Set;

import com.google.common.collect.ImmutableSet;
import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import com.google.template.soy.shared.restricted.SoyFunction;

import com.atlassian.soy.impl.functions.ConcatFunction;
import com.atlassian.soy.impl.functions.ContextFunction;
import com.atlassian.soy.impl.functions.GetTextAsHtmlFunction;
import com.atlassian.soy.impl.functions.GetTextFunction;
import com.atlassian.soy.impl.functions.IsListFunction;
import com.atlassian.soy.impl.functions.IsMapFunction;

/**
 * Our custom soy functions are added here
 */
class CoreFunctionsModule extends AbstractModule {
    static final Set<String> CORE_FUNCTION_NAMES = ImmutableSet.of(
            ConcatFunction.FUNCTION_NAME,
            ContextFunction.FUNCTION_NAME,
            GetTextFunction.FUNCTION_NAME,
            GetTextAsHtmlFunction.FUNCTION_NAME,
            IsMapFunction.FUNCTION_NAME,
            IsListFunction.FUNCTION_NAME);

    @Override
    public void configure() {
        Multibinder<SoyFunction> binder = Multibinder.newSetBinder(binder(), SoyFunction.class);
        binder.addBinding().to(ConcatFunction.class);
        binder.addBinding().to(ContextFunction.class);
        binder.addBinding().to(GetTextFunction.class);
        binder.addBinding().to(GetTextAsHtmlFunction.class);
        binder.addBinding().to(IsMapFunction.class);
        binder.addBinding().to(IsListFunction.class);
    }
}
