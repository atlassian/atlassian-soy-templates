package com.atlassian.soy.renderer;

import com.atlassian.annotations.PublicApi;

/**
 * The type of sanitization previously applied to a {@link SanitizedString string}
 *
 * @since 2.6
 */
@PublicApi
public enum SanitizationType {
    /**
     * A value which can safely be included in a style tag and evaluated as CSS
     *
     * @since 3.0
     */
    CSS,
    /**
     * A value which can safely be included in a script tag and thus executed as JavaScript without a syntax error
     * or an XSS vulnerability
     *
     * @since 3.0
     */
    JS,
    /**
     * A value which can safely be included in a JavaScript string without causing a syntax error or
     * an XSS vulnerability
     *
     * @since 3.0
     */
    JS_STRING,
    /**
     * A value which can safely be included in a HTML document without causing a syntax error or
     * an XSS vulnerability
     */
    HTML,
    /**
     * A value which can safely be included in a HTML tag attribute without causing a syntax error or
     * an XSS vulnerability
     */
    HTML_ATTRIBUTE,
    /**
     * A value which is not sanitized and is merely plain text
     *
     * @since 3.0
     */
    TEXT,
    /**
     * A value which can safely be included in a URI without causing the URI to be malformed
     */
    URI
}
