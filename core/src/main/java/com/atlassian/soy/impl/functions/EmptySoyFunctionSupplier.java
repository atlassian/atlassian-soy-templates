package com.atlassian.soy.impl.functions;

import java.util.Collections;

import com.atlassian.soy.renderer.SoyFunction;
import com.atlassian.soy.spi.functions.SoyFunctionSupplier;

/**
 * A stub implementation of  {@link SoyFunctionSupplier} which returns no functions
 *
 * @since 2.4
 */
public class EmptySoyFunctionSupplier implements SoyFunctionSupplier {
    @Override
    public Iterable<SoyFunction> get() {
        return Collections.emptySet();
    }
}
