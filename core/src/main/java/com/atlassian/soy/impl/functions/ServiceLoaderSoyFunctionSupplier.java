package com.atlassian.soy.impl.functions;

import java.util.ServiceLoader;

import com.google.common.collect.ImmutableList;

import com.atlassian.soy.renderer.SoyFunction;
import com.atlassian.soy.spi.functions.SoyFunctionSupplier;

public class ServiceLoaderSoyFunctionSupplier implements SoyFunctionSupplier {
    private final ServiceLoader<SoyFunction> serviceLoader;

    public ServiceLoaderSoyFunctionSupplier() {
        this.serviceLoader = ServiceLoader.load(SoyFunction.class);
    }

    public ServiceLoaderSoyFunctionSupplier(ClassLoader classLoader) {
        this.serviceLoader = ServiceLoader.load(SoyFunction.class, classLoader);
    }

    @Override
    public Iterable<SoyFunction> get() {
        return ImmutableList.copyOf(serviceLoader.iterator());
    }
}
