# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [7.2.0]
### Added
- [BORG-779](https://bulldog.internal.atlassian.com/browse/BORG-779) Support for Java records
- [BORG-999](https://bulldog.internal.atlassian.com/browse/BORG-999) Support for explicit getters in Java records

## [7.1.0] - 2024-08-15
### Added
- [DCA11Y-1177](https://hello.jira.atlassian.cloud/browse/DCA11Y-1177) HTML traceability comments

## [7.0.6] - 2024-08-06
### Changed
- Updating dependencies

## [7.0.5] - 2024-07-22
### Changed
- [CONFSRVDEV-32176](https://jira.atlassian.com/browse/CONFSRVDEV-32176) Add Velocity Allowlist entry to allow
  `DefaultSoyTemplateRenderer#render` to be invoked from a Velocity template

## [7.0.4] - 2024-08-06
### Changed
- Updating dependencies

## [7.0.3] - 2024-06-13
### Changed
- [DCA11Y-1065](https://hello.jira.atlassian.cloud/browse/DCA11Y-1065) Add js association verification

## [7.0.2] - 2024-05-22
### Changed
- [DCPL-1511](https://ecosystem.atlassian.net/browse/DCPL-1511) Remove Guava from Public API

## [7.0.1] - 2024-05-07
### Changed
- Updated to stable platform 7.0.0

## [7.0.0] - 2024-04-16
### Changed
- [BSP-3810](https://bulldog.internal.atlassian.com/browse/BSP-3810) Upgrade spring version to 5.3.18.
- [BSP-3851](https://bulldog.internal.atlassian.com/browse/BSP-3851) upgrade platform to 6.0.3 and atlassian-plugin to 5.7.10
- [BSP-4518](https://bulldog.internal.atlassian.com/browse/BSP-4518) Upgrade commons-text version to 1.10.0
- [DCPL-392](https://ecosystem.atlassian.net/browse/DCPL-392) Remove usage of `com.atlassian.annotations.tenancy.TenantAware` annotations

## [6.1.0] - 2023-12-21
### Changed
- [DCPL-484](https://ecosystem.atlassian.net/browse/DCPL-484)[DCPL-483](https://ecosystem.atlassian.net/browse/DCPL-483) Deprecated and removed Guava

## [6.0.8] - 2024-07-17
### Changed
- Updating dependencies

## [6.0.7] - 2023-07-11
### Changed
- [BSP-5179](https://https://bulldog.internal.atlassian.com/browse/BSP-5179) Upgrade to Guava 32.0.0-jre

## [6.0.6] - 2023-06-13
### Changed
- Updating dependencies

## [6.0.5] - 2022-07-27
### Changed
- [CONFSRVDEV-23900](https://jira.atlassian.com/browse/CONFSRVDEV-23900) Upgrade to AMPS 8.6.0 to run with Confluence 8.0

## [6.0.0-m04] - 2022-02-02
### Changed
- [BSP-3810](https://bulldog.internal.atlassian.com/browse/BSP-3810) Upgrade spring version to 5.3.18.
- [BSP-3851](https://bulldog.internal.atlassian.com/browse/BSP-3851) upgrade platform to 6.0.3 and atlassian-plugin to 5.7.10

## [6.0.0-m03] - 2021-12-20
### Changed
Bumped Platform Version to 6.0.0-m04 for Google Guava update
[BSP-3452](https://bulldog.internal.atlassian.com/browse/BSP-3452) Release Atlassian Soy Templates with Guava update

## [5.4.0] - 2021-11-03
### Changed
- [ITAS-163](https://bulldog.internal.atlassian.com/browse/ITAS-163) Add metric to the render method of the soy template renderer. The metric name is "soyTemplateRenderer"

## [5.3.2] - 2021-10-21
### Changed
- Updated platform version to 5.3.5 and added missing events (BSP-4137)

## [5.3.1] - 2021-10-21
### Changed
- [SPFE-855](https://ecosystem.atlassian.net/browse/SPFE-855) upmerged

## [5.3.0] - 2021-06-18
### Changed
- [BSP-2822](https://bulldog.internal.atlassian.com/browse/BSP-2822) Migrating Spring Framework from 5.1.20.RELEASE to 5.3.7 and Spring Boot from 2.1.18.RELEASE to 2.5.1.

## [5.2.1] - 2021-10-21
### Changed
- [SPFE-855](https://ecosystem.atlassian.net/browse/SPFE-855) upmerged

## [5.2.0] - 2021-06-11
### Changed
- [VULN-266301](https://asecurityteam.atlassian.net/browse/VULN-266301) upmerged
- [SOY-122](https://ecosystem.atlassian.net/browse/SOY-122) Unquieten exceptions during dev mode to retain cause of
  errors in messages

## [5.1.2] - 2021-10-21
### Changed
- [SPFE-855](https://ecosystem.atlassian.net/browse/SPFE-855) Remove web-panel render time events
- [VULN-266301](https://asecurityteam.atlassian.net/browse/VULN-266301) Update com.fasterxml.jackson.core:jackson-databind
  to 2.10.5.1. [Jackson 2.10 release notes](https://github.com/FasterXML/jackson/wiki/Jackson-Release-2.10)

## [5.1.1] - 2021-05-17
### Changed
- [SPFE-492](https://ecosystem.atlassian.net/browse/SPFE-492) Change WebPanelRenderTimeEvent to be asynchronously-processed
- Upmerged: Removal of platform POM

## [5.1.0] - 2021-02-03
### Changed
- [SPFE-252](https://ecosystem.atlassian.net/browse/SPFE-252) Added analytic event that fires with time to render each webpanel

## [5.0.3] - 2021-04-29
### Changed
- Remove Platform POM

## [5.0.2] - 2020-12-17
### Changed
- Updated soycompiler to 20140422.26-atlassian-2
- Updated spring-boot to 2.1.18.RELEASE to get rid of Jackson 2.9 dependencies which have known security vulnerabilities

## [5.0.1] - 2020-05-13
### Changed
- Update to final Platform 5 POM

## [5.0.0] - 2018-12-11
### Changed
- Requires Java 8+
- Requires Atlassian Platform 5+
- Requires Guava 26.0-jre+
- Missing plugin modules are now a soft warning when resolving the dependency graph for template compilation (SOY-115)

## [4.5.0] - 2017-02-10
### Added
- Soy template compilations are shared between usage in plugin modules (SOY-134)

[Unreleased]: https://bitbucket.org/atlassian/atlassian-soy-templates/branches/compare/master%0Dsoy-templates-parent-7.1.0
[7.1.0]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-7.1.0..soy-templates-parent-7.0.6
[7.0.6]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-7.0.6..soy-templates-parent-7.0.5
[7.0.5]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-7.0.5..soy-templates-parent-7.0.4
[7.0.4]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-7.0.4..soy-templates-parent-7.0.3
[7.0.3]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-7.0.3..soy-templates-parent-7.0.2
[7.0.2]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-7.0.2..soy-templates-parent-7.0.1
[7.0.1]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-7.0.1..soy-templates-parent-7.0.0
[7.0.0]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-7.0.0..soy-templates-parent-6.0.8
[6.1.0]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-6.1.0..soy-templates-parent-6.0.8
[6.0.8]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-6.0.8..soy-templates-parent-6.0.7
[6.0.7]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-6.0.7..soy-templates-parent-6.0.6
[6.0.6]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-6.0.6..soy-templates-parent-6.0.5
[6.0.5]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-6.0.5..soy-templates-parent-6.0.4
[6.0.4]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-6.0.4..soy-templates-parent-6.0.3
[6.0.3]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-6.0.3..soy-templates-parent-6.0.2
[6.0.2]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-6.0.2..soy-templates-parent-6.0.1
[6.0.1]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-6.0.1..soy-templates-parent-6.0.0
[6.0.0]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-6.0.0..soy-templates-parent-5.4.0
[5.4.0]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-5.4.0..soy-templates-parent-5.3.1
[5.3.1]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-5.3.1..soy-templates-parent-5.3.0
[5.3.0]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-5.3.0..soy-templates-parent-5.2.1
[5.2.1]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-5.2.1..soy-templates-parent-5.2.0
[5.2.0]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-5.2.0..soy-templates-parent-5.1.2
[5.1.2]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-5.1.2..soy-templates-parent-5.1.1
[5.1.1]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-5.1.1..soy-templates-parent-5.1.0
[5.1.0]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-5.1.0..soy-templates-parent-5.0.2
[5.0.3]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-5.0.3..soy-templates-parent-5.0.2
[5.0.2]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-5.0.2..soy-templates-parent-5.0.1
[5.0.1]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-5.0.1..soy-templates-parent-5.0.0
[5.0.0]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-5.0.0..soy-templates-parent-4.5.0
[4.5.0]: https://bitbucket.org/atlassian/atlassian-soy-templates/compare/soy-templates-parent-4.5.0..soy-templates-parent-4.4.3
