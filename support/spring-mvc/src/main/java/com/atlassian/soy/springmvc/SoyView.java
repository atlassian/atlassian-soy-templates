package com.atlassian.soy.springmvc;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.View;
import com.google.common.base.Charsets;

import com.atlassian.soy.renderer.SoyTemplateRenderer;

/**
 * Simple View implementation for Soy. Renders using a SoyTemplateRenderer.
 *
 * @since 2.3
 */
public class SoyView implements View {

    private static final String CONTENT_TYPE = "text/html;charset=UTF-8";
    private static final String STATIC = "static";

    private final InjectedDataFactory injectedDataFactory;
    private final SoyTemplateRenderer templateRenderer;
    private final String viewName;
    private final String rootModuleKey;

    public SoyView(
            String viewName,
            SoyTemplateRenderer templateRenderer,
            InjectedDataFactory injectedDataFactory,
            String rootModuleKey) {
        this.injectedDataFactory = injectedDataFactory;
        this.templateRenderer = templateRenderer;
        this.viewName = viewName;
        this.rootModuleKey = rootModuleKey;
    }

    public SoyView(String viewName, SoyTemplateRenderer templateRenderer, InjectedDataFactory injectedDataFactory) {
        this(viewName, templateRenderer, injectedDataFactory, STATIC);
    }

    @Override
    public String getContentType() {
        return CONTENT_TYPE;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void render(Map data, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
            throws Exception {
        httpServletResponse.setContentType(getContentType());
        templateRenderer.render(
                getWriterSafely(httpServletResponse),
                rootModuleKey,
                viewName,
                data == null ? Collections.emptyMap() : (Map<String, Object>) data,
                injectedDataFactory.createInjectedData(httpServletRequest));
    }

    private PrintWriter getWriterSafely(HttpServletResponse httpServletResponse) throws IOException {
        try {
            return httpServletResponse.getWriter();
        } catch (IllegalStateException e) {
            return new PrintWriter(new OutputStreamWriter(httpServletResponse.getOutputStream(), Charsets.UTF_8));
        }
    }
}
