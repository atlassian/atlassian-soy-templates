package com.atlassian.soy.renderer;

import java.util.function.Supplier;
import javax.annotation.Nonnull;

import com.atlassian.annotations.PublicApi;
import com.atlassian.webresource.api.QueryParams;

/**
 * Resolve the {@link QueryParams} for the current web resource request. This can be used to facilitate
 * state specific {@link SoyClientFunction soy client functions}.
 *
 * @since 2.4
 */
@PublicApi
public interface QueryParamsResolver extends Supplier<QueryParams> {

    /**
     * @return the query params associated with the current web resource request
     * @throws java.lang.IllegalThreadStateException when this method is called outside of the context of a web resource request
     */
    @Nonnull
    QueryParams get();
}
