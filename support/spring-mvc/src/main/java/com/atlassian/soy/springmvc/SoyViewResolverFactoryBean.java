package com.atlassian.soy.springmvc;

import org.springframework.beans.factory.FactoryBean;

import com.atlassian.soy.renderer.SoyTemplateRenderer;

/**
 * FactoryBean for {@link SoyViewResolver}.
 * <p>
 * Example consumption:
 * <p>
 * in your web.xml:
 *
 * <pre>
 *     &#60;filter&#62;
 *         &#60;filter-name&#62;webContextProvider&#60;/filter-name&#62;
 *         &#60;filter-class&#62;org.springframework.web.filter.DelegatingFilterProxy&#60;/filter-class&#62;
 *     &#60;/filter&#62;
 * </pre>
 *
 * and in your application's $appname-mvc.xml (or whatever you call it):
 *
 * <pre>
 *     &#60;bean id="soyViewResolver" class="com.atlassian.soy.spring.SoyTemplateRendererFactoryBean"&#62;
 *         &#60;property name="soyTemplateRenderer" ref="soyTemplateRenderer"/&#62;
 *     &#60;/bean&#62;
 * </pre>
 *
 * @since 2.4
 */
public class SoyViewResolverFactoryBean implements FactoryBean<SoyViewResolver> {

    private InjectedDataFactory injectedDataFactory;
    private SoyTemplateRenderer soyTemplateRenderer;

    public void setInjectedDataFactory(InjectedDataFactory injectedDataFactory) {
        this.injectedDataFactory = injectedDataFactory;
    }

    public void setSoyTemplateRenderer(SoyTemplateRenderer soyTemplateRenderer) {
        this.soyTemplateRenderer = soyTemplateRenderer;
    }

    @Override
    public SoyViewResolver getObject() throws Exception {
        if (injectedDataFactory == null) {
            setInjectedDataFactory(new EmptyInjectedDataFactory());
        }

        return new SoyViewResolver(soyTemplateRenderer, injectedDataFactory);
    }

    @Override
    public Class<SoyViewResolver> getObjectType() {
        return SoyViewResolver.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
