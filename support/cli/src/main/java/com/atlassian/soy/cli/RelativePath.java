package com.atlassian.soy.cli;

import java.io.File;

/**
 * Container for absolute and relative paths
 */
final class RelativePath {
    /**
     * The absolute location of the file
     */
    public final File absolutePath;
    /**
     * The location of this file relative to the basePath it was found under
     */
    public final String relativePath;

    RelativePath(File absolutePath, String relativePath) {
        this.absolutePath = absolutePath;
        this.relativePath = relativePath;
    }
}
