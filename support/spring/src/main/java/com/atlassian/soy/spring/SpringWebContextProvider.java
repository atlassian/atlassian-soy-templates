package com.atlassian.soy.spring;

import java.util.Locale;

import org.springframework.context.i18n.LocaleContext;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.atlassian.soy.impl.web.SimpleWebContextProvider;
import com.atlassian.soy.spi.web.WebContextProvider;

import static java.util.Objects.requireNonNull;

/**
 * An implementation of {@link WebContextProvider} which makes use of the Spring thread local
 * contexts for requests and locale (i.e. {@code RequestContextHolder} and {@code LocaleContextHolder}.
 * <p>
 * In the case of the thread locals not being initialized, the {@code fallbackWebContextProvider} configured
 * is used instead
 *
 * @since 4.2.0
 */
public class SpringWebContextProvider implements WebContextProvider {

    private final WebContextProvider fallbackWebContextProvider;

    public SpringWebContextProvider() {
        this(new SimpleWebContextProvider());
    }

    public SpringWebContextProvider(WebContextProvider fallbackWebContextProvider) {
        this.fallbackWebContextProvider = requireNonNull(fallbackWebContextProvider, "fallbackWebContextProvider");
    }

    @Override
    public String getContextPath() {
        ServletRequestAttributes requestAttributes = getRequestAttributes();
        if (requestAttributes == null) {
            if (fallbackWebContextProvider == null) {
                return "";
            }
            return fallbackWebContextProvider.getContextPath();
        }
        return requestAttributes.getRequest().getContextPath();
    }

    @Override
    public Locale getLocale() {
        LocaleContext localeContext = LocaleContextHolder.getLocaleContext();
        if (localeContext == null) {
            if (fallbackWebContextProvider == null) {
                return Locale.getDefault();
            }
            return fallbackWebContextProvider.getLocale();
        }
        return localeContext.getLocale();
    }

    private ServletRequestAttributes getRequestAttributes() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes instanceof ServletRequestAttributes) {
            return (ServletRequestAttributes) requestAttributes;
        }
        return null;
    }
}
