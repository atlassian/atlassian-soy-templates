package com.atlassian.soy.impl.webpanel;

import org.junit.Test;

import com.atlassian.plugin.ModuleCompleteKey;
import com.atlassian.soy.renderer.SoyException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 */
public class TemplateAddressingTest {

    public static final String DEFAULT_PLUGIN_KEY = "defaultPluginKey";

    @Test
    public void testValidAddressing() throws Exception {
        assertValidAddress(
                "fully qualified", "pluginKey:moduleKey/soy.template.name", "pluginKey:moduleKey", "soy.template.name");

        assertValidAddress(
                "partially qualified",
                ":moduleKey/soy.template.name",
                DEFAULT_PLUGIN_KEY + ":moduleKey",
                "soy.template.name");

        assertValidAddress(
                "partially qualified with dot",
                ".:moduleKey/soy.template.name",
                DEFAULT_PLUGIN_KEY + ":moduleKey",
                "soy.template.name");
    }

    @Test
    public void testInvalidAddresses() throws Exception {
        assertInvalidAddress("must have a colon :", "moduleKey/soy.template.name");

        assertInvalidAddress("must have a single colon :", "pluginKey::moduleKey/soy.template.name");

        assertInvalidAddress("must have a single colon :", "::moduleKey/soy.template.name");

        assertInvalidAddress("must have module", ":/soy.template.name");

        assertInvalidAddress("must some key", "/soy.template.name");

        assertInvalidAddress("must slash", "soy.template.name");

        assertInvalidAddress("must slash", "plugin:module:soy.template.name");
    }

    private void assertValidAddress(
            final String reason,
            final String templateAddress,
            final String pluginKeyModuleKey,
            final String templateName) {
        try {
            ModuleCompleteKey expectedCompleteKey = new ModuleCompleteKey(pluginKeyModuleKey);
            TemplateAddressing.Address address =
                    TemplateAddressing.parseTemplateAddress(templateAddress, DEFAULT_PLUGIN_KEY);
            assertThat(reason + " -  completeKey", address.getCompleteKey(), equalTo(expectedCompleteKey));
            assertThat(reason + "- templateName", address.getTemplateName(), equalTo(templateName));
        } catch (SoyException e) {
            fail(reason + " - " + e.getMessage());
        }
    }

    private void assertInvalidAddress(final String reason, final String templateAddress) {
        try {
            TemplateAddressing.parseTemplateAddress(templateAddress, DEFAULT_PLUGIN_KEY);
            fail(reason + " - Unexpected invalid address - " + templateAddress);
        } catch (SoyException expected) {
        }
    }
}
