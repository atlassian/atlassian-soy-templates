package com.atlassian.soy.springmvc;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;

/**
 * Allows spring-mvc soy users to provide certain data in all views without having to put it into every model. Data put
 * in here will be accessible in soy templates under the namespace '$ij'
 *
 * @since 2.3
 */
public interface InjectedDataFactory {
    Map<String, Object> createInjectedData(HttpServletRequest request);
}
