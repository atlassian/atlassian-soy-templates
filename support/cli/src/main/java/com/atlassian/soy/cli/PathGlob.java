package com.atlassian.soy.cli;

/**
 * Specification of a glob
 */
class PathGlob {
    /**
     * Base directory under which files matching glob will be found
     */
    private final String baseDirectory;

    private final String include;
    private final String exclude;

    PathGlob(String baseDirectory, String glob) {
        this(baseDirectory, glob, null);
    }

    PathGlob(String baseDirectory, String include, String exclude) {
        this.baseDirectory = baseDirectory;
        this.include = include;
        this.exclude = exclude;
    }

    public String getBaseDirectory() {
        return baseDirectory;
    }

    public String getInclude() {
        return include;
    }

    public String getExclude() {
        return exclude;
    }
}
