package com.atlassian.soy.impl.functions;

import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.commons.lang3.StringUtils;
import com.google.common.collect.ImmutableSet;
import com.google.template.soy.data.SoyData;
import com.google.template.soy.data.SoyValue;
import com.google.template.soy.data.restricted.StringData;
import com.google.template.soy.internal.base.CharEscapers;
import com.google.template.soy.jssrc.restricted.JsExpr;
import com.google.template.soy.jssrc.restricted.SoyJsSrcFunction;
import com.google.template.soy.shared.restricted.SoyJavaFunction;

import com.atlassian.soy.spi.web.WebContextProvider;

/**
 * Simple function that returns the current request context to the caller.
 *
 * @since 1.0
 */
@Singleton
public class ContextFunction implements SoyJsSrcFunction, SoyJavaFunction {
    public static final String FUNCTION_NAME = "contextPath";

    private final WebContextProvider webContextProvider;
    private final boolean useAjsContextPath;

    @Inject
    public ContextFunction(
            WebContextProvider webContextProvider,
            @Named(SoyProperties.USE_AJS_CONTEXT_PATH) boolean useAjsContextPath) {
        this.webContextProvider = webContextProvider;
        this.useAjsContextPath = useAjsContextPath;
    }

    private static String stripTrailingSlash(String base) {
        return StringUtils.chomp(base, "/");
    }

    @Override
    public String getName() {
        return FUNCTION_NAME;
    }

    @Override
    public Set<Integer> getValidArgsSizes() {
        return ImmutableSet.of(0);
    }

    @Override
    public JsExpr computeForJsSrc(List<JsExpr> args) {
        String expr;
        if (useAjsContextPath) {
            expr = "AJS.contextPath()";
        } else {
            expr = '"' + CharEscapers.javascriptEscaper().escape(getContextPath()) + '"';
        }
        return new JsExpr(expr, Integer.MAX_VALUE);
    }

    @Override
    public SoyData computeForJava(List<SoyValue> soyDatas) {
        return StringData.forValue(getContextPath());
    }

    private String getContextPath() {
        return stripTrailingSlash(webContextProvider.getContextPath());
    }
}
