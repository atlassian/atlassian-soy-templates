package com.atlassian.soy.impl.functions;

import com.atlassian.soy.spi.functions.SoyFunctionSupplier;
import com.atlassian.webresource.spi.transformer.TransformerUrlBuilder;

public interface UrlEncodingSoyFunctionSupplier extends SoyFunctionSupplier, TransformerUrlBuilder {}
