package com.atlassian.soy.impl.modules;

import com.google.inject.AbstractModule;

import com.atlassian.webresource.api.WebResourceManager;

/**
 * Provides the plugin specific components for the soy functions
 */
class PluginsBridgeModule extends AbstractModule {
    private final WebResourceManager webResourceManager;

    public PluginsBridgeModule(WebResourceManager webResourceManager) {
        this.webResourceManager = webResourceManager;
    }

    @Override
    public void configure() {
        binder().bind(WebResourceManager.class).toInstance(webResourceManager);
    }
}
