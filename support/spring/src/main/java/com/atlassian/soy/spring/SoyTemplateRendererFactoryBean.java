package com.atlassian.soy.spring;

import java.util.List;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.ResourceLoader;

import com.atlassian.soy.impl.DefaultSoyTemplateRenderer;
import com.atlassian.soy.impl.SoyManager;
import com.atlassian.soy.impl.SoyManagerBuilder;
import com.atlassian.soy.impl.i18n.ResourceBundleI18nResolver;
import com.atlassian.soy.renderer.SoyDataMapper;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.soy.spi.TemplateSetFactory;
import com.atlassian.soy.spi.functions.SoyFunctionSupplier;
import com.atlassian.soy.spi.i18n.I18nResolver;
import com.atlassian.soy.spi.i18n.JsLocaleResolver;
import com.atlassian.soy.spi.modules.GuiceModuleSupplier;
import com.atlassian.soy.spi.web.WebContextProvider;

/**
 * FactoryBean for {@link SoyTemplateRenderer}. Saves consumers from needing 30 lines of xml to get it working.
 * <p>
 * Example consumption:
 * <p>
 * in your applicationContext.xml:
 * <p>
 * <pre>
 *     &#60;bean id="webContextProvider" class="com.atlassian.soy.spring.WebContextProviderServletFilter"/&#62;
 *     &#60;bean id="templateSetFactory" class="com.atlassian.soy.spring.ResourceLoaderTemplateSetFactory"/&#62;
 *
 *     &#60;bean id="i18nResolver" class="com.atlassian.soy.spring.MessageSourceI18nResolver"
 *          c:messageSource-ref="messageSource"
 *          c:webContextProvider-ref="webContextProvider" /&#62;
 *
 *     &#60;bean id="soyTemplateRenderer" class="com.atlassian.soy.spring.SoyTemplateRendererFactoryBean"&#62;
 *          &#60;property name="i18nResolver" ref="i18nResolver"/&#62;
 *          &#60;property name="templateSetFactory" ref="templateSetFactory"/&#62;
 *          &#60;property name="webContextProvider" ref="webContextProvider"/&#62;
 *      &#60;/bean&#62;
 * </pre>
 *
 * @since 2.4
 */
public class SoyTemplateRendererFactoryBean
        implements FactoryBean<SoyTemplateRenderer>, ResourceLoaderAware, MessageSourceAware {
    private final SoyManagerBuilder builder = new SoyManagerBuilder();

    private MessageSource messageSource;
    private ResourceLoader resourceLoader;
    private WebContextProvider webContextProvider;

    public void setWebContextProvider(WebContextProvider webContextProvider) {
        this.webContextProvider = webContextProvider;
    }

    public void setTemplateSetFactory(TemplateSetFactory templateSetFactory) {
        builder.templateSetFactory(templateSetFactory);
    }

    public void setJsLocaleResolver(JsLocaleResolver jsLocaleResolver) {
        builder.localeResolver(jsLocaleResolver);
    }

    public void setI18nResolver(I18nResolver i18nResolver) {
        builder.i18nResolver(i18nResolver);
    }

    public void setModuleSupplier(GuiceModuleSupplier moduleSupplier) {
        builder.moduleSupplier(moduleSupplier);
    }

    @Override
    public void setMessageSource(MessageSource messageSource) {
        if (this.messageSource == null) {
            this.messageSource = messageSource;
        }
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        if (this.resourceLoader == null) {
            this.resourceLoader = resourceLoader;
        }
    }

    public void setSoyDataMappers(List<SoyDataMapper<?, ?>> soyDataMappers) {
        builder.dataMappers(soyDataMappers);
    }

    public void setSoyFunctionSupplier(SoyFunctionSupplier soyFunctionSupplier) {
        builder.functionSupplier(soyFunctionSupplier);
    }

    @Override
    public SoyTemplateRenderer getObject() throws Exception {
        if (webContextProvider == null) {
            setWebContextProvider(new SpringWebContextProvider());
        }
        builder.webContextProvider(webContextProvider);

        if (!builder.hasI18nResolver()) {
            setI18nResolver(
                    messageSource == null
                            ? new ResourceBundleI18nResolver(webContextProvider)
                            : new MessageSourceI18nResolver(webContextProvider, messageSource));
        }

        if (!builder.hasTemplateSetFactory()) {
            final ResourceLoaderTemplateSetFactory resourceLoaderTemplateSetFactory =
                    new ResourceLoaderTemplateSetFactory();
            resourceLoaderTemplateSetFactory.setResourceLoader(resourceLoader);
            setTemplateSetFactory(resourceLoaderTemplateSetFactory);
        }

        SoyManager soyManager = builder.build();

        return new DefaultSoyTemplateRenderer(soyManager);
    }

    @Override
    public Class<SoyTemplateRenderer> getObjectType() {
        return SoyTemplateRenderer.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
