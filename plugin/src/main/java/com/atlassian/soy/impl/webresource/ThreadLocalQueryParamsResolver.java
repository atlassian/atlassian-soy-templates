package com.atlassian.soy.impl.webresource;

import javax.annotation.Nonnull;

import com.google.common.base.Supplier;

import com.atlassian.soy.renderer.QueryParamsResolver;
import com.atlassian.webresource.api.QueryParams;

public class ThreadLocalQueryParamsResolver implements QueryParamsResolver {

    private final ThreadLocal<QueryParams> currentQueryParams = new ThreadLocal<>();

    @Nonnull
    @Override
    public QueryParams get() {
        QueryParams queryParams = currentQueryParams.get();

        if (queryParams == null) {
            throw new IllegalThreadStateException(QueryParamsResolver.class.getName()
                    + " cannot be invoked outside the context of a web resource transformation.");
        }

        return queryParams;
    }

    <T> T withQueryParams(@Nonnull QueryParams value, Supplier<T> callback) {
        QueryParams original = currentQueryParams.get();
        currentQueryParams.set(value);
        try {
            return callback.get();
        } finally {
            if (original == null) {
                currentQueryParams.remove();
            } else {
                currentQueryParams.set(original);
            }
        }
    }
}
