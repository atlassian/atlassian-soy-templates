package com.atlassian.soy.springmvc.errors;

import javax.annotation.Nullable;

import com.google.common.base.MoreObjects;
import com.google.common.base.Throwables;

/**
 * Convenience error methods.
 *
 * @since 2.3
 */
public class ErrorUtils {

    protected ErrorUtils() {
        throw new UnsupportedOperationException(
                getClass().getName() + " is a utility class and should not be instantiated");
    }

    /**
     * @param throwable the throwable to extract a localised root cause message from
     * @return the localized message of the root cause of {@code throwable}
     */
    @Nullable
    public static String getLocalizedMessageOfRootCause(@Nullable Throwable throwable) {
        if (throwable == null) {
            return null;
        }

        Throwable cause = Throwables.getRootCause(throwable);
        return MoreObjects.firstNonNull(cause.getLocalizedMessage(), cause.toString());
    }
}
