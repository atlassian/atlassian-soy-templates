package com.atlassian.soy.impl.functions;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import io.atlassian.util.concurrent.ResettableLazyReference;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.soy.renderer.SoyClientFunction;
import com.atlassian.soy.renderer.SoyFunction;
import com.atlassian.soy.renderer.SoyFunctionModuleDescriptor;
import com.atlassian.soy.spi.functions.SoyFunctionSupplier;
import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.spi.transformer.TransformerUrlBuilder;

/**
 * An implementation of {@link SoyFunctionSupplier} which finds SoyFunction supplied by the plugin system
 *
 * @since 2.4
 */
public class PluginSoyFunctionSupplier implements UrlEncodingSoyFunctionSupplier {

    private static final Predicate<ModuleDescriptor<?>> IS_SOY_FUNCTION =
            descriptor -> descriptor instanceof SoyFunctionModuleDescriptor;
    private static final Function<SoyFunctionModuleDescriptor, SoyFunction> TO_MODULE =
            SoyFunctionModuleDescriptor::getModule;

    private final PluginAccessor pluginAccessor;
    private final PluginEventManager pluginEventManager;

    private final Set<SoyFunctionModuleDescriptor> moduleDescriptors;

    // UrlState contains list of plugin versions and list of module descriptors
    private final ResettableLazyReference<UrlState> state;

    public PluginSoyFunctionSupplier(PluginAccessor pluginAccessor, PluginEventManager pluginEventManager) {
        this.pluginAccessor = pluginAccessor;
        this.pluginEventManager = pluginEventManager;

        moduleDescriptors = new CopyOnWriteArraySet<>();
        state = new ResettableLazyReference<UrlState>() {
            @Override
            protected UrlState create() {
                return buildState();
            }
        };
    }

    public void registerListeners() {
        pluginEventManager.register(this);
        addDescriptors(pluginAccessor.getEnabledModuleDescriptorsByClass(SoyFunctionModuleDescriptor.class));
    }

    public void unregisterListeners() {
        pluginEventManager.unregister(this);
    }

    @Override
    public Iterable<SoyFunction> get() {
        return Iterables.transform(moduleDescriptors, TO_MODULE);
    }

    @Override
    public void addToUrl(UrlBuilder urlBuilder) {
        state.get().addToUrl(urlBuilder);
    }

    @PluginEventListener
    @SuppressWarnings("unchecked")
    public void onPluginModuleEnabled(PluginModuleEnabledEvent event) {
        addDescriptors((Collection) Collections2.filter(Collections.singleton(event.getModule()), IS_SOY_FUNCTION));
    }

    @PluginEventListener
    @SuppressWarnings("unchecked")
    public void onPluginModuleDisabled(PluginModuleDisabledEvent event) {
        removeDescriptors((Collection) Collections2.filter(Collections.singleton(event.getModule()), IS_SOY_FUNCTION));
    }

    @PluginEventListener
    @SuppressWarnings("unchecked")
    public void onPluginDisabled(PluginDisabledEvent event) {
        removeDescriptors((Collection) Collections2.filter(event.getPlugin().getModuleDescriptors(), IS_SOY_FUNCTION));
    }

    private void addDescriptors(Collection<SoyFunctionModuleDescriptor> descriptors) {
        if (moduleDescriptors.addAll(descriptors)) {
            state.reset();
        }
    }

    private void removeDescriptors(Collection<SoyFunctionModuleDescriptor> descriptors) {
        if (moduleDescriptors.removeAll(descriptors)) {
            state.reset();
        }
    }

    private UrlState buildState() {
        // Use a linked hash map for consistent ordering and for reasonable toString
        // implementation
        Map<String, String> pluginKeysToVersions = new LinkedHashMap<>();
        ImmutableSet.Builder<SoyFunctionModuleDescriptor> statefulFunctionDescriptors = ImmutableSet.builder();

        for (SoyFunctionModuleDescriptor descriptor : moduleDescriptors) {
            if (!SoyClientFunction.class.isAssignableFrom(descriptor.getModuleClass())) {
                continue;
            }

            // Collect the key and version of contributing plugins
            pluginKeysToVersions.put(
                    descriptor.getPlugin().getKey(),
                    descriptor.getPlugin().getPluginInformation().getVersion());

            // Allow each soy function the opportunity to contribute to the URL
            // if they don't implement TransformerUrlBuilder, assume they are
            // pure functions
            if (TransformerUrlBuilder.class.isAssignableFrom(descriptor.getModuleClass())) {
                statefulFunctionDescriptors.add(descriptor);
            }
        }

        return new UrlState(pluginKeysToVersions, statefulFunctionDescriptors.build());
    }

    private static class UrlState implements TransformerUrlBuilder {

        private final String globalState;
        private final Iterable<SoyFunctionModuleDescriptor> statefulFunctionDescriptors;

        UrlState(
                Map<String, String> pluginKeyToVersion,
                Iterable<SoyFunctionModuleDescriptor> statefulFunctionDescriptors) {
            this.globalState = pluginKeyToVersion.toString();
            this.statefulFunctionDescriptors = statefulFunctionDescriptors;
        }

        @Override
        public void addToUrl(UrlBuilder urlBuilder) {
            urlBuilder.addToHash("soyGlobalState", globalState);
            for (SoyFunctionModuleDescriptor descriptor : statefulFunctionDescriptors) {
                ((TransformerUrlBuilder) descriptor.getModule()).addToUrl(urlBuilder);
            }
        }
    }
}
