package com.atlassian.soy.renderer;

import com.atlassian.webresource.spi.transformer.TransformerUrlBuilder;

/**
 * A specific kind of {@link SoyClientFunction} that is url contributing and dimension aware,
 * so resources executing this function can be pre-baked using provided dimensions
 * ({@code com.atlassian.webresource.api.prebake.Dimensions}), i.e., states it can assume.
 *
 * @since 4.3.0
 */
public interface StatefulSoyClientFunction extends TransformerUrlBuilder, SoyClientFunction {}
