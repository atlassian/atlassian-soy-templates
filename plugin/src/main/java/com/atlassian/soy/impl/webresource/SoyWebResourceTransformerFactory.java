package com.atlassian.soy.impl.webresource;

import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.soy.impl.functions.UrlEncodingSoyFunctionSupplier;
import com.atlassian.webresource.api.transformer.TransformerParameters;
import com.atlassian.webresource.spi.transformer.TransformerUrlBuilder;
import com.atlassian.webresource.spi.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.webresource.spi.transformer.WebResourceTransformerFactory;

/**
 * @since 2.4
 */
public class SoyWebResourceTransformerFactory implements WebResourceTransformerFactory {
    private final SoyTransformerUrlBuilder soyTransformerUrlBuilder;
    private final SoyWebResourceTransformer soyWebResourceTransformer;
    private final WebResourceIntegration webResourceIntegration;
    private final UrlEncodingSoyFunctionSupplier soyFunctionSupplier;

    public SoyWebResourceTransformerFactory(
            SoyTransformerUrlBuilder soyTransformerUrlBuilder,
            SoyWebResourceTransformer soyWebResourceTransformer,
            WebResourceIntegration webResourceIntegration,
            UrlEncodingSoyFunctionSupplier soyFunctionSupplier) {
        this.soyTransformerUrlBuilder = soyTransformerUrlBuilder;
        this.soyWebResourceTransformer = soyWebResourceTransformer;
        this.webResourceIntegration = webResourceIntegration;
        this.soyFunctionSupplier = soyFunctionSupplier;
    }

    @Override
    public UrlReadingWebResourceTransformer makeResourceTransformer(TransformerParameters ignored) {
        return soyWebResourceTransformer;
    }

    @Override
    public TransformerUrlBuilder makeUrlBuilder(TransformerParameters ignored) {
        return soyTransformerUrlBuilder;
    }
}
