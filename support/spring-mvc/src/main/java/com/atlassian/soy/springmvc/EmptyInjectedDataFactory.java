package com.atlassian.soy.springmvc;

import java.util.Collections;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

/**
 * Default implementation of InjectedDataFactory, provides an empty map.
 *
 * @since 2.3
 */
public class EmptyInjectedDataFactory implements InjectedDataFactory {
    @Override
    public Map<String, Object> createInjectedData(HttpServletRequest request) {
        return Collections.emptyMap();
    }
}
