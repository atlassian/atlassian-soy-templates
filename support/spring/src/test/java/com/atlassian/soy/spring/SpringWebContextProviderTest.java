package com.atlassian.soy.spring;

import java.util.Locale;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.i18n.SimpleLocaleContext;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.junit.After;
import org.junit.Assume;
import org.junit.Test;

import com.atlassian.soy.impl.web.SimpleWebContextProvider;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class SpringWebContextProviderTest {

    private SpringWebContextProvider webContextProvider = new SpringWebContextProvider();

    @After
    public void resetThreadState() {
        RequestContextHolder.resetRequestAttributes();
        LocaleContextHolder.resetLocaleContext();
    }

    @Test
    public void getContextPathShouldPreferenceTheContextPathOfTheCurrentRequest() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        String contextPath = "/some/context/path";
        request.setContextPath(contextPath);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        assertThat(webContextProvider.getContextPath(), equalTo(contextPath));
    }

    @Test
    public void getContextPathShouldUseFallbackContextPathWhenNoCurrentRequest() throws Exception {
        assertThat(RequestContextHolder.getRequestAttributes(), nullValue());
        assertThat(webContextProvider.getContextPath(), equalTo(""));
    }

    @Test
    public void getContextPathShouldUseFallbackContextPathConfiguredWhenNoCurrentRequest() throws Exception {
        assertThat(RequestContextHolder.getRequestAttributes(), nullValue());
        webContextProvider = new SpringWebContextProvider(new SimpleWebContextProvider("/blah/blah"));
        assertThat(webContextProvider.getContextPath(), equalTo("/blah/blah"));
    }

    @Test
    public void getContextPathShouldUseFallbackContextPathWhenRequestContextHolderNotServletRequestAttributes()
            throws Exception {
        RequestContextHolder.setRequestAttributes(mock(RequestAttributes.class));
        assertThat(webContextProvider.getContextPath(), equalTo(""));
    }

    @Test
    public void getLocaleShouldPreferenceTheLocaleInContext() throws Exception {
        Assume.assumeThat(Locale.getDefault(), not(equalTo(Locale.TRADITIONAL_CHINESE)));
        LocaleContextHolder.setLocaleContext(new SimpleLocaleContext(Locale.TRADITIONAL_CHINESE));
        assertThat(webContextProvider.getLocale(), equalTo(Locale.TRADITIONAL_CHINESE));
    }

    @Test
    public void getLocaleShouldUseTheSystemLocaleWhenNoLocaleContextTheLocaleInContext() throws Exception {
        assertThat(LocaleContextHolder.getLocaleContext(), nullValue());
        assertThat(webContextProvider.getLocale(), equalTo(Locale.getDefault()));
    }
}
