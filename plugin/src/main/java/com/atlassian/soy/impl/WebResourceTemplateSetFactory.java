package com.atlassian.soy.impl;

import java.io.IOException;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Charsets;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableSet;
import com.google.common.io.Resources;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.soy.spi.TemplateSetFactory;
import com.atlassian.webresource.api.descriptor.WebResourceModuleDescriptor;

import static com.atlassian.soy.impl.DevMode.isDevMode;

/**
 * Determines and caches the appropriate set of template file URLs for different sets of plugin modules.
 *
 * @since 2.3
 */
class WebResourceTemplateSetFactory implements TemplateSetFactory {
    private static final Logger log = LoggerFactory.getLogger(WebResourceTemplateSetFactory.class);

    private final PluginAccessor pluginAccessor;
    private final ServletContextFactory servletContextFactory;

    /**
     * Requisite config so we can make an Injector and a Builder without parsing all the modules twice.
     */
    private final LoadingCache<String, Set<URL>> templateSetCache;

    public WebResourceTemplateSetFactory(PluginAccessor pluginAccessor, ServletContextFactory servletContextFactory) {
        this.pluginAccessor = pluginAccessor;
        this.servletContextFactory = servletContextFactory;
        CacheLoader<String, Set<URL>> findTemplatesFunction = new CacheLoader<String, Set<URL>>() {
            @Override
            public Set<URL> load(String moduleKey) throws IOException {
                return findRequiredTemplates(moduleKey);
            }
        };

        templateSetCache = CacheBuilder.newBuilder().build(findTemplatesFunction);
    }

    @Override
    public Set<URL> get(String completeModuleKey) {
        return templateSetCache.getUnchecked(completeModuleKey);
    }

    @Override
    public void clear() {
        templateSetCache.invalidateAll();
    }

    private Set<URL> findRequiredTemplates(String pluginModuleKey) throws IOException {
        log.debug("Found Soy template files for '{}'", pluginModuleKey);
        TemplateSetBuilder templateSetBuilder = new TemplateSetBuilder();

        templateSetBuilder.addTemplatesForTree(pluginModuleKey);

        // Check for potential problems
        List<String> missingModuleDescriptors = templateSetBuilder.missingModuleDescriptors();
        if (!missingModuleDescriptors.isEmpty()) {
            log.warn(
                    "Some module descriptors are either missing or disabled; soy compilation may fail."
                            + " Missing descriptors: {}",
                    missingModuleDescriptors);
        }

        // Return a set of soy templates to use in compilation
        Set<URL> result = templateSetBuilder.build();
        log.debug("Found Soy template files for '{}' was {}", pluginModuleKey, result);
        return result;
    }

    private class TemplateSetBuilder {
        private final Set<URL> fileSet = new HashSet<>();
        private final Set<String> alreadyAddedModules = new HashSet<>();
        private final List<String> missingDescriptors = new ArrayList<>();

        private void addTemplatesForTree(String completeModuleKey) throws IOException {
            if (alreadyAddedModules.contains(completeModuleKey)) {
                return;
            }
            alreadyAddedModules.add(completeModuleKey);

            ModuleDescriptor<?> moduleDescriptor = pluginAccessor.getEnabledPluginModule(completeModuleKey);

            if (moduleDescriptor == null) {
                missingDescriptors.add(completeModuleKey);
                log.debug("Required plugin module " + completeModuleKey + " was either missing or disabled");
            } else {

                if (moduleDescriptor instanceof WebResourceModuleDescriptor) {
                    addTemplatesForTree((WebResourceModuleDescriptor) moduleDescriptor);
                }

                addSoyTemplateResources(moduleDescriptor);
            }
        }

        private void addTemplatesForTree(WebResourceModuleDescriptor webResourceModuleDescriptor) throws IOException {
            for (String dependencyModuleKey : webResourceModuleDescriptor.getDependencies()) {
                addTemplatesForTree(dependencyModuleKey);
            }
        }

        private void addSoyTemplateResources(ModuleDescriptor<?> moduleDescriptor) throws IOException {
            for (ResourceDescriptor resource : moduleDescriptor.getResourceDescriptors()) {
                if (isSoyTemplate(resource)) {
                    URL url = getSoyResourceURL(moduleDescriptor, resource);
                    if (url != null) {
                        if (isDevMode()) {
                            try (Reader reader =
                                    Resources.asCharSource(url, Charsets.UTF_8).openStream()) {
                                if (!reader.ready()) {
                                    throw new IOException("Empty file for resource " + resource.getLocation()
                                            + " in module descriptor " + moduleDescriptor.getCompleteKey());
                                }
                            }
                        }
                        this.fileSet.add(url);
                    }
                }
            }
        }

        private boolean isSoyTemplate(ResourceDescriptor resource) {
            return StringUtils.endsWith(resource.getLocation(), ".soy");
        }

        private URL getSoyResourceURL(ModuleDescriptor moduleDescriptor, ResourceDescriptor resource) {
            final String sourceParam = resource.getParameter("source");
            if ("webContextStatic".equalsIgnoreCase(sourceParam)) {
                try {
                    return servletContextFactory.getServletContext().getResource(resource.getLocation());
                } catch (MalformedURLException e) {
                    log.error("Ignoring soy resource. Could not locate soy with location: " + resource.getLocation());
                    return null;
                }
            }
            return moduleDescriptor.getPlugin().getResource(resource.getLocation());
        }

        private Set<URL> build() {
            return ImmutableSet.copyOf(fileSet);
        }

        private List<String> missingModuleDescriptors() {
            return missingDescriptors;
        }
    }
}
