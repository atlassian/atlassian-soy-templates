package com.atlassian.soy.impl.functions;

import java.util.Collections;
import java.util.Set;

import com.atlassian.soy.renderer.JsExpression;
import com.atlassian.soy.renderer.SoyClientFunction;
import com.atlassian.soy.renderer.SoyServerFunction;

/**
 * A trivial soy function which helps to bridge the gap between Atlassian Soy 2.x and 3.x by allowing
 * consumers to call toString on the specified object
 * <p>
 * Atlassian Soy 3.x no longer calls toString on objects when printing POJOs. To support templates that
 * rely on this functionality, this method can be called as a substitute
 *
 * @since 2.9
 */
public class ToStringFunction implements SoyServerFunction<String>, SoyClientFunction {
    private static final Set<Integer> VALID_ARG_SIZES = Collections.singleton(1);

    @Override
    public String getName() {
        return "toString";
    }

    @Override
    public Set<Integer> validArgSizes() {
        return VALID_ARG_SIZES;
    }

    @Override
    public JsExpression generate(JsExpression... args) {
        return new JsExpression("'' + " + args[0].getText());
    }

    @Override
    public String apply(Object... args) {
        return String.valueOf(args[0]);
    }
}
