package com.atlassian.soy.spi.modules;

import com.google.common.base.Supplier;
import com.google.inject.Module;

/**
 * SPI for declaring what guice modules should be made available to soy.
 *
 * @since 2.3
 */
public interface GuiceModuleSupplier extends Supplier<Iterable<Module>> {
    /**
     * @return the provided guice modules.
     */
    @Override
    Iterable<Module> get();
}
